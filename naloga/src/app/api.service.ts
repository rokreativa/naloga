import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiUrl = "https://jsonplaceholder.typicode.com/";

  constructor(private http: HttpClient) { }

  public getDoctors(): Observable<any> {
    return this.http.get(this.apiUrl + 'users');
  }

  public getAllTasks(): Observable<any> {
    return this.http.get(this.apiUrl + 'todos');
  }

  public getTasksForDoctor(doctorId: number): Observable<any> {
    return this.http.get(this.apiUrl + `users/${doctorId.toString()}/todos`);
  }
}
