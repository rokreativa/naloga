import {Component, OnInit} from '@angular/core';
import {ApiService} from "./api.service";
import {lastValueFrom} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public docs: Array<any> = [];

  constructor(private api: ApiService) {
  }

  ngOnInit() {
   this.getDoctors();
  }

  public async getDoctors() {
    const doctors$ = this.api.getDoctors();
    this.docs = await lastValueFrom(doctors$);
  }

  public checkAllData(docI: number) {
    this.docs[docI].showDetails = !this.docs[docI].showDetails;
  }

  public async showTasks(docId: number) {
    const docTasks$ = this.api.getTasksForDoctor(docId);
    const docIndex = this.docs.findIndex(doc => doc.id == docId);

    if (!this.docs[docIndex].todos || !this.docs[docIndex].todos.length) {
      try {
        this.docs[docIndex].todos = await lastValueFrom(docTasks$);
      } catch (e) {
        alert("Error - check console for details.");
        console.error(e);
      }
    }

    this.docs[docIndex].showTodos = true;
  }
}
